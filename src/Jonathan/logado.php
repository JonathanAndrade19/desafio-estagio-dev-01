<?php

session_start();

if(!isset($_SESSION['usuario'])){
    header('Location: ../../index.php?sessao_expirada=1');
    die();
}

if($_SESSION['usuario']['tipo'] == 1){
    echo 'Meu nome é '.$_SESSION['usuario']['nome'].' e sou usuário Administrador do Sistema';
}else if($_SESSION['usuario']['tipo'] == 2){
    echo 'Meu nome é '.$_SESSION['usuario']['nome'].' e sou usuário Comum do Sistema';
}else{
    header('Location: ../../index.php?permissao=false');
    die();
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Você está Logado</title>
</head>
<body>
    <a href="sair.php">Sair</a>
</body>
</html>