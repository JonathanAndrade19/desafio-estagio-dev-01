CREATE DATABASE desafio;

USE desafio;

CREATE TABLE IF NOT EXISTS `desafio`.`usuarios` (
  `nome` VARCHAR(100) CHARACTER SET 'utf8' NOT NULL,
  `email` VARCHAR(100) CHARACTER SET UNIQUE 'utf8' NOT NULL,
  `senha` VARCHAR(100) CHARACTER SET 'utf8' NOT NULL,
  `tipo` INT(100) CHARACTER SET 'utf8' NOT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`));

  INSERT INTO usuarios(nome,email,senha,tipo) VALUES ("jonathan","jonathan@emial.com",MD5("1234567J"),1);
  INSERT INTO usuarios(nome,email,senha,tipo) VALUES ("edmo","edmo@emial.com",MD5("0123456E"),2);
  INSERT INTO usuarios(nome,email,senha,tipo) VALUES ("lidia","lidia@emial.com",MD5("0123456L"),1);