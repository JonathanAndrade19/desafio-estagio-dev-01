<?php
    // Conectando o Banco de Dados
    require_once 'MyPDO.php';

    // Validação de Acesso.
    if(isset($_POST['email']) && isset($_POST['senha'])){
        $email = $_POST['email'];
        $senha = $_POST['senha'];

        if(!senhaValida($senha)){
            header("Location: ../../index.php?senha_invalida=1");
            die();
        }

        // Validando email e senha
        try{
            $conexao = new MyPDO();

            $senha = md5($senha);
            
            $stmt = $conexao->query(
                "SELECT * FROM usuarios WHERE email = '$email' AND senha = '$senha'"
            );
            $usuario = $stmt->fetch(PDO::FETCH_ASSOC);

            $conexao = null;

            if($usuario == false){
                header('Location: ../../index.php?erro=1');
                die();
            }

            session_start();
            $_SESSION['usuario'] = $usuario;
            header('Location: logado.php');
            die();
            
        } catch(Exception $e){
            echo 'Erro ao se Conectar com o Banco: '.$e->getMessage();
        }
    }

    function senhaValida($senha){
         return preg_match('/[A-Z]/', $senha)  
             && preg_match('/[0-9]/', $senha) 
             && preg_match('/^[\w$@]{8,}$/', $senha); 
    }